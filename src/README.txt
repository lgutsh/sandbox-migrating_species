Migrating Species - algorytm genetyczny z migracją pomiędzy procesami.

Zależności:
mpich2
gcc 4.8.2

Budowanie:
W katalogu ze źródłami (po zainstalowaniu mpich2)
mpic++ -I. -std=c++11 ./test.cpp

Uruchamianie:
    - (na jednej maszynie):
mpiexec -n 5 ./a.out
    - (na wielu maszynach):
mpiexec --hosts=localhost,otherhost ./a.out
