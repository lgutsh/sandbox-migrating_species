#include <mpich/mpi.h>

#include <Population.hpp>

using namespace std;

int main(int argc, char ** argv)
{
    // inicjalizacja MPI
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    cout<<"main(): rank: "<<rank<<" size: "<<size<<endl;
  
    // symulacja
    Simulation simul;
    simul.wsize() = size;
    simul.wrank() = rank;
    simul.start();

    // zamykanie MPI
    int mpiReturnVal = MPI_Finalize();
    cout<<"main(): end of simulation in: "<<rank<<endl;
    return mpiReturnVal;
}
