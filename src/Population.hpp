#pragma once

#include <vector>
#include <iostream>
#include <cmath>
#include <random>

#include <Species.hpp>
#include <MigratoryImpl.hpp>

using namespace std;

/**
 * @class Population Implementuje populację osobników o zadanej charakterystyce (interfejsie).
 * Stworzona dla wygody - określa, z których interfejsów będziemy korzystać oraz jest kontenerem do przechowywania elementów gatunku (tutaj wektor).
 * @param S Klasa osobników.
 */
template <typename S = Species>
struct Population
        : public std::vector<S>
        , public Mutable<S>
        , public Crossoverable<S>
        , public FitnessCalculable<S>
        , public MigratoryImpl<S>
{

    /**
     * Konstruktor pobiera funktory definiujące zachowanie
     */
    Population(
            typename Mutable<S>::Mutator mutator,
            typename Crossoverable<S>::Cross cross,
            typename FitnessCalculable<S>::FitnessCalculator calculator
               )
        : Mutable<S>(mutator)
        , Crossoverable<S>(cross)
        , FitnessCalculable<S>(calculator)
        , mGeneration(0)
    {
        std::cout<<"P"<<std::endl;
    }

protected:
    // Klasa Simulation ma dostęp do funkcji protected (z interfejsów Mutable, Crossoverable, etc.)
    // Nie zaburza to enkapsulacji - alternatywnie Population i Simulation mogłyby być jedną dużą klasą albo Population mogłaby być zagnieżdżona w Simulation.
    friend class Simulation;
    
    /**
     * Zwraca referencję do generacji populacji. Aktualizowana w klasie Simulation.
     */
    uint64_t & generation() { return mGeneration; }

    // Która generacja?
    uint64_t mGeneration;
};

/**
 * @class Simulation Implementuje algorytm genetyczny na populacji.
 */
struct Simulation
{
    /**
     * Domyślny konstruktor.
     */
    Simulation();
    /**
     * Start symulacji.
     */
    void start();
    /**
     * Wartość rank w mpich.
     */
    int & wrank() { return mPopulation->wrank(); }
    /**
     * Wartość world size w mpich.
     */
    int & wsize() { return mPopulation->wsize(); }
protected:
    /**
     * Populacja.
     */
    Population<Species> * mPopulation;
    /**
     * Funktor z symulacją.
     */
    function<uint64_t(Population<Species> &)> mSimula;
};

Simulation::Simulation()
{
    /*
     * Poniżej implementacja całego zachowania.
     * Poszczególne funktory definiują zachowanie populacji.
     */
    // sposób mutacji: losowo zmieniamy x i y w zadanym zakresie zależnym od generacji (referencja do populacji):
    auto mutate = [&](Species & s) -> Species &
    {
//        cout<<"mutate"<<endl;

        std::random_device rd;
        std::mt19937 gen(rd());
        double delta = min(0.3, 10.0/mPopulation->generation());
        std::uniform_real_distribution<> dis(-delta, delta);

        double deltaX = dis(gen);
        double deltaY = dis(gen);

        s.x += deltaX;
        s.y += deltaY;

        return s;
    };

    // krzyżowanie: bierzemy x z pierwszego a y z drugiego rodzica:
    auto cross = [](Species & a, Species & b) -> Species
    {
//        cout<<"cross"<<endl;

        Species s;
        s.x = a.x;
        s.y = b.y;
        return s;
    };

    // fitness - wartość funkcji (maksymalizujemy)
    typedef FitnessCalculable<Species>::fitness_t fitness_t;
    auto fitness = [](Species & s) -> fitness_t
    {
        /*
          * We will do here simple function on <0,1> that have several local maxima.
          * for example (x**2 + y**2)*( 1+sin(10*x)*sin(20*y) );
          * f(x,y) >= 0 :)
          */
        fitness_t ret = (s.x*s.x + s.y*s.y) * (1 + sin(10*s.x)*sin(20*s.y));

        return ret;
    };

    mPopulation = new Population<Species>(mutate, cross, fitness);

    /*
      * Pojedynczy przelot symulacji (jedna generacja)
      * Mutacja -> Rozmnażanie -> Fitness -> Selekcja -> Migracja -> ...
      */
    auto singleRun = [](Population<Species> & p) -> uint64_t
    {
//        cout<<"Population size at begin of sequence: "<<p.size()<<endl;

        Species superIndividual;
        /*
          * Mutate
          */
        for(Species & s : p)
        {
            p.mutate(s);
        }
        // fall into abyss...
//        cout<<"fall into abyss"<<endl;
        for(Population<Species>::iterator i = p.begin();;)
        {
            if(i == p.end()) break;

            Species & s = *i;
            if(s.x < 0 || s.x > 1 || s.y < 0 || s.y > 1)
            {
                p.erase(i);
            }
            else
            {
                ++i;
            }
        }
        for(Species & s : p)
        {
            s.fitness = p.calculateFitness(s);
            if(s.fitness > superIndividual.fitness) superIndividual = s;
        }


        /*
         * Crossover
         */
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> dis(0, p.size()-1);

        // how many babies in one run
        constexpr uint32_t breedRate = 20;
        int32_t diff = (100 - p.size() + breedRate);
        uint32_t currentRate =  diff> 0 ? diff : 0;

        for(uint32_t i = 0; i < currentRate; ++i)
        {
            Species newIndividual = p.crossover(p[dis(gen)], p[dis(gen)]);
            newIndividual.fitness = p.calculateFitness(newIndividual);
            if(newIndividual.fitness > superIndividual.fitness) superIndividual = newIndividual;
            p.push_back(newIndividual);
        }

        cout<<"Super individual: \tx="<<superIndividual.x<<"\ty="<<superIndividual.y<<"\tfitness="<<superIndividual.fitness<<"\t"<<p.wrank()<<endl;

        /*
          * Select
          */
        uint32_t deathRate = 0;

        std::random_device rdev;
        std::mt19937 killerGen(rdev());
        std::uniform_real_distribution<> deathDistribution(0.0, 1.0);

        uint loopLimit = 5;
        while(deathRate < breedRate)
        {
            if(loopLimit == 0)
            {
                p.erase(p.begin(), p.begin() + breedRate - deathRate);
                cout<<"reaching loop limit, size:"<<p.size()<<endl;
                break;
            }

            for(Population<Species>::iterator i = p.begin();;)
            {
                if(i == p.end()) break;
                Species & s = *i;

                bool toKill = false;
                // kill small
                if(s.fitness/superIndividual.fitness < deathDistribution(killerGen))
                {
                    toKill = true;
                }

                // change mind if it is very different
                if(toKill && (s.x - superIndividual.x)*(s.x - superIndividual.x) + (s.y - superIndividual.y)*(s.y - superIndividual.y) > deathDistribution(killerGen))
                {
                    toKill = false;
                }

                if(toKill)
                {
                    p.erase(i);
                    ++deathRate;
                }
                else
                {
                    ++i;
                }
                if (deathRate >= breedRate) break;
//                cout<<"Death rate: "<<deathRate<<endl;
            }
            --loopLimit;
        }

        /*
          * Migrate
          */
        bool emSuc = false;
        emSuc = p.emigrate(superIndividual);
        for(int j = 0; j < p.wsize() -1; ++j) {
        Species i;
        bool imSuc = p.immigrate(i);
//        cout<<"Population size at end of sequence: "<<p.size()<<" ems="<<emSuc<<" ims="<<imSuc<<" wsize:"<<p.wsize()<<endl;
        p.push_back(i);
        }
    };

    mSimula = singleRun;

}

void Simulation::start()
{
    // populate with random data:
    uint32_t initialIndividualNumber = 100;
    while(initialIndividualNumber--)
    {
        mPopulation->push_back(Species());
    }

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0);

    // initial x,y
    for(Species & s : *mPopulation)
    {
        s.x = dis(gen);
        s.y = dis(gen);
        s.fitness = mPopulation->calculateFitness(s);
    }

    // uruchamiamy symulację na dwieście pokoleń:
    while(++mPopulation->generation() <= 200)
    {
//        cout<<"Generation: "<<mPopulation->generation()<<" ::::::::";
        mSimula(*mPopulation);
    }
//    for(Species & s : *mPopulation)
//    {
//        cout<<"individual: x="<<s.x<<" y="<<s.y<<" fitness="<<s.fitness<<endl;
//    }

    mPopulation->stop();
}
