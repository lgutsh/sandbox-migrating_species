#pragma once

#include <functional>


/**
 * @class Species Struktura osobników gatunku
 * 
 * Ta implementacja to punkt na płaszczyźnie, gdyż będziemy szukać maksimum funkcji dwóch zmiennych.
 * Dodatkowe pole do przechowywania wartości funkcji fitness.
 */
struct Species
{
    /**
     * @typedef fitness_t Typ wartości fitness.
     */     
    typedef double fitness_t;
    /**
     * Wartość składowej x.
     */     
    long double x;
    /**
     * Wartość składowej y.
     */     
    long double y;
    /**
     * Wartość fitness.
     */     
    fitness_t fitness;
    /**
      * Sygnalizuje koniec symulacji.
      */
    char gameover;
};

/*
 * --------------------------------------------------------------------------------------------------------------
 * Poniższe szablony są parametryzowane klasą gatunku (domyślnie powyższą - ale niekoniecznie).                 -
 * Populacja może implementować poniższe interfejsy aby stworzyć algorytm optymalizacji bazujący na nich.       -
 * Funkcje interfejsu są specjalnie typu 'protected', gdyż nie ma potrzeby ich wykorzystywania publicznie       -
 * --------------------------------------------------------------------------------------------------------------
 */

/**
 * @class Mutable<S> Interfejs mutowania osobników gatunku S.
 * @param S Klasa gatunku, której osobniki mają być mutowane.
 * 
 * Populacja musi dziedziczyć po Mutable<S>, jeśli mutacja jest dozwolona.
 * Sposób mutacji - właściwa implementacja - jest podawana w konstruktorze (lambda, std::function lub wskaźnik).
 *
 */
template <typename S = Species>
struct Mutable
{
    /**
     * @typedef Mutator sygnatura funkcji mutacji.
     */     
    typedef std::function<S &(S &)> Mutator;
    /**
     * Konstruktor pobierający funktor z implementacją.
     * @param mutator Funktor implementujący właściwe zachowanie.
     */     
    Mutable(Mutator mutator) { mMutator = mutator; }
protected:
    /**
     * Funkcja mutująca element (powinna zwracać referencję do tego samego elementu - możnaby ją zmienić na void).
     * @param prototype Osobnik do zmutowania.
     */
    S & mutate(S & prototype) { return mMutator(prototype); }

protected:
    Mutator mMutator;
};


/**
 * @class Crossoverable<S> Interfejs rozmnażania osobników gatunku S.
 * @param S Klasa gatunku, której osobniki mają być rozmnażane.
 * 
 * Populacja musi dziedziczyć po Crossoverable<S>, jeśli rozmnażanie jest dozwolone.
 * Sposób rozmnażania - właściwa implementacja - jest podawany w konstruktorze (lambda, std::function lub wskaźnik).
 *
 */
template <typename S = Species>
struct Crossoverable
{
    /**
     * @typedef Cross sygnatura funkcji rozmnażania.
     */     
    typedef std::function<S(S &, S &)> Cross;
    /**
     * Konstruktor pobierający funktor z implementacją.
     * @param mutator Funktor implementujący właściwe zachowanie.
     */     
    Crossoverable(Cross cross) { mCross = cross; }
protected:
    /**
     * Funkcja krzyżująca dwa osobniki.
     * @param parentA Rodzic 1.
     * @param parentB Rodzic 2.
     * @return Dziecko.
     */
    S crossover(S & parentA, S & parentB) { return mCross(parentA, parentB); } 

protected:
    Cross mCross;
};

/**
 * @class Migratory<S> Interfejs migracji osobników gatunku S.
 * @param S Klasa gatunku, której osobniki mają migrować.
 * 
 * Populacja musi dziedziczyć po Migratory<S>, jeśli migracja jest dozwolona.
 * Sposób migracji - właściwa implementacja - jest podawana w konstruktorze (lambda, std::function lub wskaźnik).
 *
 */
template <typename S = Species>
struct Migratory
{
    /**
     * @typedef Migrator sygnatura funkcji migracji.
     */     
    typedef std::function<bool(S &)> Migrator;
    /**
     * Konstruktor pobierający funktor z implementacją.
     * @param mutator Funktor implementujący właściwe zachowanie.
     */     
    Migratory(Migrator emigrator, Migrator immigrator) { mEmigrator = emigrator; mImmigrator = immigrator; }
protected:
    /**
     * Funkcja emigracji.
     * @param individual Osobnik emigrujący.
     * @return true jeśli się udało.
     */
    bool emigrate(S & individual) { return mEmigrator(individual); }
    /**
     * Funkcja imigracji.
     * @param individual Osobnik imigrujący.
     * @return true jeśli się udało.
     */
    bool immigrate(S & individual) { return mImmigrator(individual); }

protected:
    Migrator mEmigrator;
    Migrator mImmigrator;
};

/**
 * @class FitnessCalculable<S> Interfejs funkcji fitness osobników gatunku S.
 * @param S Klasa gatunku, dla osobników którego obliczana jest funkcja fitness.
 * 
 * Populacja musi dziedziczyć po FitnessCalculable<S>, aby obliczyć fitness.
 * Właściwa implementacja - jest podawana w konstruktorze (lambda, std::function lub wskaźnik).
 *
 */
template <typename S = Species>
struct FitnessCalculable
{
    /**
     * @typedef fitness_t typ wartości fitness.
     */     
    typedef Species::fitness_t fitness_t;
    /**
     * @typedef Mutator sygnatura funkcji fitness.
     */     
    typedef std::function<fitness_t(S &)> FitnessCalculator;
    /**
     * Konstruktor pobierający funktor z implementacją.
     * @param mutator Funktor implementujący właściwe zachowanie.
     */     
    FitnessCalculable(FitnessCalculator calculator) { mFitnessCalculator = calculator; }
protected:
    /**
     * Funkcja obliczania fitness.
     * @param individual Osobnik, dla którego funkcja fitness ma być obliczona.
     * @return Wartość funkcji fitness.
     */
    fitness_t calculateFitness(S & individual) { return mFitnessCalculator(individual); }

protected:
    FitnessCalculator mFitnessCalculator;
};

