#pragma once

#include <thread>
#include <mutex>

#include <mpich/mpi.h>

#include <Species.hpp>

/**
 * @class MigratoryImpl Implementacja migracji z użyciem mpich2
 */
template <typename S = Species>
struct MigratoryImpl : public Migratory<S>
{
    /**
     * Konstruktor domyślny.
     */
    MigratoryImpl();

    /**
     * Koniec symulacji. Wyłącza warunek pętli wątku i czeka na zakończenie w wątku wywołania.
     */
    void stop();
    /**
     * Wartość "world rank" z MPI.
     */
    int & wrank() { return mRank; }

    /**
     * Wartość "world size" z MPI.
     */
    int & wsize() { return mSize; }

protected:
    // kolejka odebranych osobników
    std::vector<S> mReceived;
    // kolejka osobników do wysłania
    std::vector<S> mToBeSent;

    // mutexy dostępu do kolejek
    std::mutex mRecMutex;
    std::mutex mSendMutex;

    // wątek MPI
    std::thread mMPIThread;
    // funkcja główna wątku
    std::function<void()> mMPIThreadFunc;

    // warunek stopu
    volatile bool mRunning;
    bool mPreparingToStop;

    // stopień migracji
    static constexpr int MIGRATION_RATE = 10;
private:
    // rank, size z MPI
    int mRank, mSize;
};

template <typename S>
MigratoryImpl<S>::MigratoryImpl()
    : Migratory<S>
      (
        // emigrator
        [this](Species & s) -> bool
        {
            std::lock_guard<std::mutex> aLock(mSendMutex);
            if(mToBeSent.size() == MIGRATION_RATE) return false;
            mToBeSent.push_back(s);
            return true;
        },
        // immigrator
        [this](Species & a) -> bool
        {
            std::lock_guard<std::mutex> aLock(mRecMutex);
            if (mReceived.empty()) return false;
            S s = mReceived.back(); // get last so it's faster
            mReceived.pop_back();
            a = s;
//            std::cout<<"size mRec after pop: "<<mReceived.size()<<std::endl;
            return true;
        }
     )
    , mRunning(true)
    , mPreparingToStop(false)
{
    // funkcja wątku MPI
    auto mpiThreadFunc = [this]()
    {
        // address of dispatcher
        const int root = 0;

        while(mRunning)
        {
//            // warunek stopu:

//            char stop = mPreparingToStop;
//            char stops[wsize()];
//            for(char & s : stops) s = 0;
//            MPI_Barrier(MPI_COMM_WORLD);
//            MPI_Allgather(&stop, 1, MPI_CHAR, stops, 1, MPI_CHAR, MPI_COMM_WORLD);
//            bool end = stop;
//            for(char & s : stops)
//            {
//                end |= (s != 0);
//            }
//            MPI_Barrier(MPI_COMM_WORLD);
//            if(end)
//            {
//                mRunning = false;
//                continue;
//            }
//            // koniec warunku stopu

            S send;
            send.x = -1.0;
            send.y = -1.0;
            send.gameover = mPreparingToStop;
            S received[wsize()];
            for (S & r : received) {
                r.x = -1.;
                r.y = -1.;
                r.gameover = 0;
            }

            {
                {
                    std::lock_guard<std::mutex> aLock(mSendMutex);
                    if(mToBeSent.size())
                    {
                        send = mToBeSent.back();
                        mToBeSent.pop_back();
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            std::cout<<"~GHR~"<<wrank()<<std::endl;

            MPI_Barrier(MPI_COMM_WORLD);
            MPI_Allgather(&send, sizeof(S), MPI_CHAR, received, sizeof(S), MPI_CHAR, MPI_COMM_WORLD);
            bool end = false;
            for(S & r : received)
            {
                if(r.x > 0.0 && r.x > 0.0)
                {
                    std::lock_guard<std::mutex> aLock(mRecMutex);
                    std::cout<<"received "<<r.x<<"*"<<r.y<<" from "<<wrank()<<std::endl;
                    mReceived.push_back(r);
                    end |= r.gameover;
                }

            }
            std::cout<<"received done "<<wrank()<<std::endl;
            MPI_Barrier(MPI_COMM_WORLD);
            if(end)
            {
                mRunning = false;
                continue;
            }
            std::cout<<"barrier passed "<<wrank()<<std::endl;

        }
        // Bariera, żeby wszystkie wątki skończyły
        std::cout<<"~END~"<<wrank()<<std::endl;
        MPI_Barrier(MPI_COMM_WORLD);
        std::cout<<"+END+"<<wrank()<<std::endl;
        std::cout<<"Exiting thread number: "<<mRank<<std::endl;
    };

    // uruchamiamy wątek MPI:
    mMPIThread = std::thread(mpiThreadFunc);
}

template <typename S>
void MigratoryImpl<S>::stop()
{
    mPreparingToStop = true;
    std::cout<<"-END-"<<std::endl;
    if (mMPIThread.joinable()) mMPIThread.join();
    std::cout<<"*END*"<<std::endl;
}
